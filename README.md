Note: The readme is in english and portuguese

------------------------------------------------------------------------------------
To run:

Right button click on the project:
Run As -> Maven clean
Run As -> Maven install
-------------------------------------------------- ---------------------------------
Run unit tests
Run As -> Maven test
If you are using STS, go to Boot Dashboard, select the minimalist server and
click on the first little red square button to start
-------------------------------------------------- ---------------------------------

You can also test with Postman, Insomnia or Curl

Heroku Endpoint: https://cadastro-clientes-pip.herokuapp.com/crud-clientes/cliente

Heroku Swagger Endpoint: https://cadastro-clientes-pip.herokuapp.com/crud-clientes/swagger-ui/

NOTA: Não esquecer de incluir a "/" no fim do endpoint acima

Bitbucket: https://bitbucket.org/valdyrtorres/cadastro-clientes-pip

Create customer - POST on heroku:
https://cadastro-clientes-pip.herokuapp.com/crud-clientes/cliente

Body example (JSON):

{
"name": "Manuel Neuer",
"CPF": "11111111112",
"address": "Rua das Marrecas, 42, CEP: 22222-220, Rio de Janeiro, RJ - Brazil"
}
-------------------------------------------------- ---------------------------------
List customers - GET
https://cadastro-clientes-pip.herokuapp.com/crud-clientes/cliente
-------------------------------------------------- ---------------------------------
List customer by id - GET - Obs: 5 is an example of id
http: // localhost: 8081 / crud-clients / client / 5
-------------------------------------------------- ---------------------------------
Update customer - PUT
http: // localhost: 8081 / crud-clients / client

Body example (JSON) - You have to provide the id, where the other fields are modifiable
{
"id": 1,
"name": "Selene Silva Changed",
"CPF": "16011111112",
"address": "Av Passos, 42, CEP: 25222-220, Rio de Janeiro, RJ - Brazil"
}
-------------------------------------------------- ---------------------------------
Delete customer - DELETE - Obs: 31 is an example of id
https://cadastro-clientes-pip.herokuapp.com/crud-clientes/cliente/31
-------------------------------------------------- ---------------------------------
----------------------------------------------------------------------------------
Api Rest para Cadastro de Clientes em Java

Para rodar:

Botão direito no projeto: 
Run As -> Maven clean
Run As -> Maven install
-----------------------------------------------------------------------------------
Rodar os testes unitários
Run As -> Maven test
Se você estiver usando o STS, vá para Boot Dashboard, selecione o servidor minimalista e 
clique no primeiro botãozinho quadrado vermelho para iniciar
-----------------------------------------------------------------------------------

Pode testar também com o Postman, Insomnia ou Curl

Endpoint Heroku: https://cadastro-clientes-pip.herokuapp.com/crud-clientes/cliente

Bitbucket: https://bitbucket.org/valdyrtorres/cadastro-clientes-pip

Criar Cliente - POST no heroku:
https://cadastro-clientes-pip.herokuapp.com/crud-clientes/cliente

Exemplo de Body (JSON):

{
	"nome": "Manuel Neuer",
	"CPF": "11111111112",
	"endereco": "Rua das Marrecas, 42, CEP: 22222-220, Rio de Janeiro, RJ - Brasil"
}
-----------------------------------------------------------------------------------
Listar clientes - GET
https://cadastro-clientes-pip.herokuapp.com/crud-clientes/cliente
-----------------------------------------------------------------------------------
Listar Cliente por id - GET - Obs: 5 é um exemplo de id
http://localhost:8081/crud-clientes/cliente/5
-----------------------------------------------------------------------------------
Atualizar Cliente - PUT
http://localhost:8081/crud-clientes/cliente

Exemplo de body (JSON) - Tem que fornecer o id, onde os demais campos são modificáveis
{
	"id": 1,
	"nome": "Selene Silva Alterado",
	"CPF": "16011111112",
	"endereco": "Av Passos, 42, CEP: 25222-220, Rio de Janeiro, RJ - Brasil"
}
-----------------------------------------------------------------------------------
Deletar Cliente - DELETE - Obs: 31 é um exemplo de id
https://cadastro-clientes-pip.herokuapp.com/crud-clientes/cliente/31
-----------------------------------------------------------------------------------